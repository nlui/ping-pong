import pygame


def check_controllers_status(key_press_event,
    paddle1rect, paddle2rect, paddle_speed):
    """Handles Keydown events
    This function reacts to the button pushed events and will adjust the
    position of the paddles accordingly"""

    if key_press_event.key == 97 or \
    pygame.key.get_pressed()[97] == True:  # if the user has pressed 'a'
        paddle1rect = paddle1rect.move([0, -paddle_speed])
    if key_press_event.key == 122 or \
    pygame.key.get_pressed()[122] == True:  # if the user has pressed 'z'
        paddle1rect = paddle1rect.move([0, paddle_speed])
    if key_press_event.key == 107 or \
    pygame.key.get_pressed()[107] == True:  # if the user has pressed 'k'
        paddle2rect = paddle2rect.move([0, -paddle_speed])
    if key_press_event.key == 109 or \
    pygame.key.get_pressed()[109] == True:  # if the user has pressed 'm'
        paddle2rect = paddle2rect.move([0, paddle_speed])
    return paddle1rect, paddle2rect
