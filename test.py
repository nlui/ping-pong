#This is just a file that is used to quickly test out various functions by
#taking advantage of the fact that it is in the same folder.
#You can delete this file without any harm to the project. But don't do it:)

import sys, pygame
pygame.init()

#screen characteristics
size = width, height = 500, 500
ballspeed = [1, 1]
paddlespeed = 2
black = 0, 0, 0

screen = pygame.display.set_mode(size)
#init the game variables and all
font_name = pygame.font.get_default_font()
font_path = pygame.font.match_font(font_name)
font = pygame.font.Font(font_path, 50)

#inite the ball and its components
ballimage = pygame.image.load("media\\ball.gif")
ball = ballimage.get_rect()
ball.center = size[0] / 2, size[1] / 2

#inite the paddle and its components
paddleimage = pygame.image.load("media\\paddle.gif")
paddle = paddleimage.get_rect()
paddle = paddle.move([200, height / 2])

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    ball = ball.move(ballspeed)
    if ball.left < 0 or ball.right > width:
        ballspeed[0] = -ballspeed[0]
    if ball.top < 0 or ball.bottom > height:
        ballspeed[1] = -ballspeed[1]

    screen.fill(black)
    screen.blit(ballimage, ball)
    pygame.display.flip()
