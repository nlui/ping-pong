

def check_for_collision(screen_size, score, ball_speed, ball_rect,
    paddle1rect, paddle2rect):
    """Checks for collision of all game objects and reacts
    1. Checks collision between either paddle and the ball
    2. Checks for collision of the ball with the top and the bottom sides
    3. Checks to make sure the ball is not out of bounds to the left or
        the right"""

    if ball_rect.colliderect(paddle1rect) == True:  # paddle1 collision
        ball_speed[0] = abs(ball_speed[0])
    elif ball_rect.colliderect(paddle2rect) == True:  # paddle2 collision
        ball_speed[0] = -abs(ball_speed[0])

    #Check collision with top and bottom sides
    if ball_rect.center[1] < 0 + ball_rect.height / 2:
        ball_speed[1] = abs(ball_speed[1])
    elif ball_rect.center[1] > screen_size[1] - (ball_rect.height / 2):
        ball_speed[1] = -abs(ball_speed[1])

    #if ball is too far on either the left or right, reset it to the middle

    if ball_rect.center[0] < 0:
        ball_rect.center = screen_size[0] / 2, screen_size[1] / 2
        score["right"] += 1
        return ball_speed, ball_rect, score
    elif ball_rect.center[0] > screen_size[0]:
        ball_rect.center = screen_size[0] / 2, screen_size[1] / 2
        score["left"] += 1
        return ball_speed, ball_rect, score

    return ball_speed, None, score
